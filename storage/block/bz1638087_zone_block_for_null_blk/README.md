# storage/block/bz1638087_zone_block_for_null_blk
Storage: bz1638087_zone_block_for_null_blk

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
